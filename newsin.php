<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">最新消息</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">最</span>
						<span class="elepageTit--word">新</span>
						<span class="elepageTit--word">消</span>
						<span class="elepageTit--word">息</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="newsli.php" class="eleTab eleTab--in">
							最新消息
						</a>
						<a href="newsli.php" class="eleTab">
							促銷活動
						</a>
						<a href="newsli.php" class="eleTab">
							媒體報導
						</a>
					</article>

					<!-- 最新消息內容區 -->
					<div class="panewsinArea">
						<div class="panewslist">
							<div class="panewslist-label">
								<h1 class="panewslist-label--red">2020</h1>
								<h1 class="panewslist-label--black">07.04</h1>
							</div>
							<h2 class="panewslistTit">最新消息標題最新消息標題最新消息標題最新消息標題</h2>
						</div>

						<div class="panewsinText">
							<!-- 文編區 -->
							<h4>文編區</h4>
						</div>

						<!-- 分享區 -->
						<article class="panewsinShare">
							<!-- 社群分享插件放這 -->
						</article>
					</div>

					<div class="modBtarea mt-40">
						<a href="newsli.php" class="btnRedBt modBtarea-backBt">回最新消息列表</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     