<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">最新消息</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">最</span>
						<span class="elepageTit--word">新</span>
						<span class="elepageTit--word">消</span>
						<span class="elepageTit--word">息</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="newsli.php" class="eleTab eleTab--in">
							最新消息
						</a>
						<a href="newsli.php" class="eleTab">
							促銷活動
						</a>
						<a href="newsli.php" class="eleTab">
							媒體報導
						</a>
					</article>

					<!-- 最新消息區 -->
					<!-- 一頁20個 -->
					<div class="panewsliArea">
						<!-- 一個最新消息 -->
						<a href="newsin.php" class="panewslist">
							<div class="panewslist-label">
								<h1 class="panewslist-label--red">2020</h1>
								<h1 class="panewslist-label--black">07.04</h1>
							</div>
							<h2 class="panewslistTit">最新消息標題最新消息標題最新消息標題最新消息標題</h2>
						</a>
						<!-- 一個最新消息 -->
						<a href="newsin.php" class="panewslist">
							<div class="panewslist-label">
								<h1 class="panewslist-label--red">2020</h1>
								<h1 class="panewslist-label--black">07.04</h1>
							</div>
							<h2 class="panewslistTit">最新消息標題最新消息標題最新消息標題最新消息標題</h2>
						</a>
					</div>
				</div>

				<!-- 跳轉頁面區 -->
				<section class="elePage mtb-20">
					<a href="javascript:void(0);" class="elePage-arrowBt elePage-arrowBt--back">
						<img src="images/back-icon.png" alt="">
					</a>
					<span class="elePage-conut">1 OF 213</span>
					<a href="javascript:void(0);" class="elePage-arrowBt elePage-arrowBt--next">
						<img src="images/next-icon.png" alt="">
					</a>
				</section>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     