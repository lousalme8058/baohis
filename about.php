<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">關於我們</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">關</span>
						<span class="elepageTit--word">於</span>
						<span class="elepageTit--word">我</span>
						<span class="elepageTit--word">們</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="about.php" class="eleTab eleTab--in">
							關於我們
						</a>
						<a href="about.php" class="eleTab">
							關於我們
						</a>
						<a href="about.php" class="eleTab">
							關於我們
						</a>
					</article>
				
					<div class="paaboutArea">
						<div class="paaboutTextArea">
						<!-- 文編區 -->
						<h4>關於我們文編區</h4>
						</div>
					
					
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     