<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">訂購完成</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">訂</span>
						<span class="elepageTit--word">購</span>
						<span class="elepageTit--word">完</span>
						<span class="elepageTit--word">成</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width paCartFinHeight">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<div class="paCartFinArea">

						<!-- 訂購成功 -->
						<h3 class="mb-40">訂購成功</h3>
						<p>
							購物完成，您可到<a href="membership.php" class=" typo-danger typo-underline">會員專區＞購物訂單</a>中，查詢商品配送進度，<br>
							如有問題歡迎多多利用客服專線，再次感謝您的購買！
						</p>

						<!-- 訂購失敗 -->
						<!-- <h3 class="mb-40">訂購失敗</h3>
						<p>
							非常抱歉，訂購失敗，請至<a href="membership.php" class="typo-danger typo-underline">會員專區＞購物訂單</a>
							，重新付款一次！
						</p> -->
				
					</div>

					<div class="modBtarea mt-30">
						<a href="index.php" class="btnBlackBt modBtarea-nextBt mb-10">回首頁</a>
						<a href="membership.php" class="btnRedBt modBtarea-backBt mb-10">會員專區</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

			<!-- 元素動畫 -->
			<img src="images/asset-34.png" alt="img" class="elepageAniArea02 wow">
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     