<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
<!-- 商品輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<link href="vendor/Owl/owl.theme.default.css" rel="stylesheet" type="text/css" media="all"> 
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		autoplay: true,
		autoplayTimeout: 5000,
		nav: false,
		loop: true,
		responsive: {
		320: {
			items: 1,
			dots: true,
			dotsEach: true
		},
		}
	})
})
</script>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">免運組合</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">免</span>
						<span class="elepageTit--word">運</span>
						<span class="elepageTit--word">組</span>
						<span class="elepageTit--word">合</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 產品介紹區 -->
					<div class="paProinArea">

						<!-- 照片輪播及訂購區 -->
						<article class="paProinTopArea">
							<section class="paProinTopArea-imgArea owl-carousel owl-theme">
								<!-- 產品圖輪播區 -->
								<img src="images/pro01.png" alt="產品圖">
								<img src="images/proin-img01.png" alt="產品圖">
							</section>
							<!-- 訂購區 -->
							<section class="paProinTopArea-infoArea">
								<h1 class="paProin-proTit mb-10">江湖牛肉拌麵</h1>
								<h3 class="paProin-specTit">帶某子鬥陣，頭家兩碗牛肉拌麵，加辣！</h3>
								<div class="paProin-redLine"></div>
								<div class="elespecArea">
									<h6 class="mb-15">規格與說明</h6>
									<p>規格：四入一袋<br>帶某子鬥陣，頭家兩碗牛肉拌麵，加辣！</p>
								</div>
								<div class="elespecArea">
									<h6 class="mb-8">
										原價
										<span class="paProin-OriPriceTit">$500</span>
									</h6>
									<h1 class="paProin-proTit">$400</h1>
								</div>
								<!-- 電腦板購買按鈕 -->
								<div class="elespecArea paProinBuyArea--com">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter pb-20">
											<label>選擇購買數量<span class="fieverti"></span></label>
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
									<a href="javascript:void(0);" class="btnRedBt">
										加入購物車
									</a>
								</div>
								<!-- 手機板購買按鈕置底 -->
								<div class="paProinBuyArea paProinBuyArea--phone">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter" >
											<label>選擇購買數量<span class="fieverti"></span></label>
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
									<a href="javascript:void(0);" class="btnRedBt">
										加入購物車
									</a>
								</div>
							</section>
							<div class="clear"></div>
						</article>

						<!-- 文編區 -->
						<article class="paProintextArea">
							<!-- 文編放這裡 -->
							<img src="images/text-img01.jpg" alt="文編圖" style="width: 100%; height: auto;">
						</article>
					</div>
				</div>
			</div>

			<div class="paProinFooterArea">
				<!-- footer -->
				<?php require('footer.php') ?>
			</div>

            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     