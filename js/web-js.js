/*網站區js*/
$(document).ready(function() {
	
	// console.log("以載入");

	/*03*/
	/*小視口導覽列開合*/
	$(".jsNavSml-bt").on('click',function(){
		// console.log("我有觸發click事件");
		
		$(".jsNavSmall").animate({
		  left: '0',
		});
		// $(".jsNavSml-openbg").show();
	});
	$(".jsNavSml-bt--close").on('click',function(){
		// console.log("我有觸發關閉事件");
		$(".jsNavSmall").animate({
			left: '-320',
		});
		$(".jsNavSml-openbg").fadeOut(500);
	});
	// $(".jsNavSml-openbg").on('click',function(){
	// 	// console.log("我有觸發關閉事件");
	// 	$(".jsNavSmall").animate({
	// 		left: '-320',
	// 	});
	// 	$(".jspatSmlNavNote").animate({
	// 		left: '-320',
	// 	});
	// 	$(".jsNavSml-openbg").fadeOut(500);
	// });

	/* 滑動出現*/
	$(window).scroll(function() {
		// alert(123);
		if($(".patSmlHeader").css("display") == "none" && $(window).scrollTop() >= 300){
			$(".patSmlHeader").show();
		}else if($(window).scrollTop()<= 300 && $(window).width() >= 1280){
			$(".patSmlHeader").hide();
		}
	});


	/*小視口導覽列以及大視口導覽列第二層開合特效*/
	$(".jsfirst-level-area").stop(true).click(function(){
		//alert("123");
		// if($(this).children(".second_level_area").css("display") == "none" && $(".nav_link").width() > 960){
		// 	$(".first_level_area > .second_level_area").slideUp(200);
  //           $(this).children(".second_level_area").slideDown(200);
		// }
		if($(this).children(".jssecond-level-area").css("display") == "none" ){
			$(".jsfirst-level-area > .jsecond-level-area").slideUp(200);
            $(this).children(".jssecond-level-area").slideDown(200);
            //$(this).removeClass("ori_style");
            //$(this).addClass("open_style");

		}else if($(this).children(".jssecond-level-area").css("display") == "block"){
			$(this).children(".jssecond-level-area").slideUp(200);
			//$(this).removeClass("open_style");
			//$(this).addClass("ori_style");
		};

	});

	//小視口通知按鈕
	$(".jsNoteBt").on('click',function(){
		// console.log("我有觸發click事件");
		$(".jsNavSmall").animate({
		  left: '-320',
		});
		$(".jspatSmlNavNote").animate({
			left: '0',
		});
		$(".jsNavSml-openbg").show();
	});
	$(".jsNavSmlNote-bt--close").on('click',function(){
		// console.log("我有觸發關閉事件");
		$(".jspatSmlNavNote").animate({
			left: '-320',
		});
		$(".jsNavSml-openbg").fadeOut(500);
	});
	// $(".jsNavSml-bt--close").on('click',function(){
	// 	// console.log("我有觸發關閉事件");
	// 	$(".jsNavSmall").animate({
	// 		left: '-320',
	// 	});
	// 	$(".jsNavSml-openbg").fadeOut(500);
	// });
	// $(".jsNavSml-openbg").on('click',function(){
	// 	// console.log("我有觸發關閉事件");
	// 	$(".jsNavSmall").animate({
	// 		left: '-320',
	// 	});
	// 	$(".jsNavSml-openbg").fadeOut(500);
	// });

	/*del icon bt關閉*/
	// $(".jsOrgViewCard-bt").on('click',function(){
	// 	$(".jsOrgViewCard").fadeToggle(500);
	// });

	// $(".jsOrgEditCard-bt").on('click',function(){
	// 	$(".jsOrgEditCard").fadeToggle(500);
	// });


	/*js dropdown arrow 特效*/
	// $(".jsClick-dropdown").click(function(){
	// 	alert("123");
	// 	$(".jsClick-dropdown--effect").css('-webkit-transform','rotate('+180+'deg)'); 
	
	// });

	

	/*按鈕according 特效*/
	$( document ).stop(true).on( "click", ".js-AccordingBt", function(){
		var get_class = $(this).prop("id");
		var get_class_number = get_class.split("-");
		var class_number = get_class_number[2];
		var get_id = "#" + "js-AccordingSection-" + class_number;
		var get_bt_id = "#" + "js-According-" + class_number;
		

		if($(get_id).css("display") == "block"){
			$(get_bt_id).removeClass("jsRotate");
			$(get_id).slideUp(300);
		}else if ($(get_id).css("display") == "none"){
			$(get_bt_id).addClass("jsRotate");
			$(get_id).slideDown(300);
		}
	});


	/* tab 切換特效*/
	$( document ).on( "click", ".jsTabBt", function(){
		var get_class = $(this).prop("id");
		var get_class_number = get_class.split("-");
		var class_number = get_class_number[1];
		var get_bt_id = "#" + "jsTabBtId-" + class_number;
		var get_id = "#" + "jsTabId-" + class_number;
	
		$(".jsTabBt").removeClass("eleTab--in");
		$(".jsTabContent").hide();
		$(get_bt_id).addClass("eleTab--in");
		$(get_id).show();
	});

	/* 案件列表開合特效*/
	$( document ).on( "click", ".js-openBt", function(){
		if($(".js-openInfo").css("display") == "block"){
			$(".js-openInfo").slideUp();
		}else{
			$(".js-openInfo").slideDown();
		}
	});

	/*改變滑鼠卷軸速度*/
	//scroll卷軸滑動速率
	/*使用passive的原因*/
	/*https://stackoverflow.com/questions/37721782/what-are-passive-event-listeners*/
	/*https://kknews.cc/zh-tw/code/amlmk5x.html*/
    function scrollAni(){
        document.addEventListener("mousewheel", function(e){
            // console.log("開始監聽");
            e.preventDefault();
        }, { passive: false });
        //DOMMouseScroll --firefox用
        document.addEventListener("DOMMouseScroll", function(e){
            // console.log("開始監聽");
            e.preventDefault();
        }, { passive: false });
        var speed = 500;
        $(window).on("mousewheel DOMMouseScroll", function(e){
            var delta = e.originalEvent.wheelDelta / 120 || - e.originalEvent.detail/3;
            var scrollTop = $(window).scrollTop();
            var finalScroll = scrollTop - delta * speed ;
            TweenMax.to($(window), 1.2, { scrollTo : { y: finalScroll }, ease: Power2.easeOut });
        });
    };

	
	//在select裡不要執行卷軸滑動速率
	$(".ui").on("mousewheel DOMMouseScroll", function(e){
        //取消冒泡事件，就不會向上傳遞
        //捕獲與冒泡文件
        //https://blog.techbridge.cc/2017/07/15/javascript-event-propagation/
        event.stopPropagation();
		console.log("滑動內卷軸");
	});

    //return 是當function在運行時，可以指定回傳一個值，當回傳值回來之後，便會跳出function，所以我們在條件判斷式中寫return false，是用來跳出該function
    var getExplorer = (function() {
        var explorer = window.navigator.userAgent,
        compare = function(s) { return (explorer.indexOf(s) >= 0); },
        ie11 = (function() { return ("ActiveXObject" in window) })();
        if (compare("MSIE") || ie11) { return 'ie'; }
        else if (compare("Firefox") && !ie11) { return 'Firefox'; }
        else if (compare("Chrome") && !ie11) { return 'Chrome'; }
        else if (compare("Opera") && !ie11) { return 'Opera'; }
        else if (compare("Safari") && !ie11) { return 'Safari'; }
    })();

    //在IE不要執行scrollAni()
    // 注意：getExplorer返回結果是一個字串，如判斷是否為IE瀏覽器
    if (getExplorer == 'ie') {
        console.log('當前瀏覽器版本：IE');  
	}else{
		scrollAni();
		console.log('觸發');  
	};
	/*改變滑鼠卷軸速度結束*/



	// 出現錨點
	// $(".jsindthumbArea").css("display","none");
	// $(window).scroll(function(){
	// 	var bannerHeight = $(".indBanner").height();
	// 	// console.log("banner高度:"+bannerHeight);
	// 	var showBtH = bannerHeight - 150;
	// 	// console.log(bannerHeight + "- 150 = "+showBtH);
	// 	// 該元素只有適口1856px出現,如果抓不到,判斷式不會觸發
	// 	var eleHeight = $(".indele-pro05").offset().top;
	// 	var winHeight = $(window).height();
	// 	// console.log(eleHeight);
	// 	// console.log(winHeight);
	// 	var hidBtH = eleHeight - winHeight + 100;
	// 	// console.log("卷軸關閉高度"+hidBtH);
	// 	// console.log($(window).scrollTop());
	// 	if($(window).scrollTop()>= showBtH && $(window).scrollTop()<= hidBtH){
	// 		$(".jsindthumbArea").css("display","block");
	// 		TweenMax.staggerTo(".jsthumb", 0, {transform: "translateY(0px)", opacity:1, delay:0}, 0.1);
	// 	}else{
	// 		// console.log("關閉");
	// 		TweenMax.to(".jsthumb", 0.5, {transform: "translateY(-50px)", opacity:0, delay:0}, 0);
	// 	}
	// });

	
		


  	
});

