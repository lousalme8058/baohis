$(document).ready(function() {
    $(".jsNavMain").html('\
        <header class="patSmlHeader"> \
            <a href="javascript:void(0);" class="jsNavSml-bt">\
                <img src="images/hambuger-icon.svg" alt="打開功能列表" width="30px" height="30px" class="patSmlHeader-navbt">\
            </a>\
            <h1 class="patSmlHeader-tit typoH1--phone">系統首頁</h1>\
            <a href="javascript:void(0);" class="elenote patSmlHeader-note jsNoteBt">\
                <span class="elenote-count">15</span>\
                <img src="images/note-icon.svg" alt="打開系統通知區" width="24px" height="24px">\
            </a>\
        </header>\
        <nav class="patSmlNav jsNavSmall">\
            <div class="patSmlNav-closeBt jsNavSml-bt--close">\
                <img src="images/pop-delete-icon.svg" alt="close button" width="30" height="30">\
            </div>\
            <br>\
            <section class="patNavMenu ptb-40 plr-30">\
                <ul class="jsfirst-level-area">\
                    <a href="overview.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="系統首頁">\
                        <img class="patNavMenu-icon" src="images/home-icon.svg" alt="系統首頁" width="20" height="20">\
                        系統首頁\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="projectli.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="案件管理">\
                        <img class="patNavMenu-icon" src="images/project-icon.svg" alt="案件管理" width="20" height="20">\
                        案件管理\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="consultantli.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="案件管理">\
                        <img class="patNavMenu-icon" src="images/consultant-icon.svg" alt="顧問管理" width="20" height="20">\
                        顧問管理\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="calendar.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="行事曆">\
                        <img class="patNavMenu-icon" src="images/calendar-icon.svg" alt="行事曆" width="20" height="20">\
                        行事曆\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="usersli.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="使用者管理">\
                        <img class="patNavMenu-icon" src="images/user-icon.svg" alt="使用者管理" width="20" height="20">\
                        使用者管理\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="logs.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="系統紀錄">\
                        <img class="patNavMenu-icon" src="images/log-icon.svg" alt="系統紀錄" width="20" height="20">\
                        系統紀錄\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <ul class="jsfirst-level-area">\
                    <a href="setting.html" class="patNavMenu-list patNavMenu-list--active pb-10" title="系統設定">\
                        <img class="patNavMenu-icon" src="images/setting-icon.svg" alt="系統設定" width="20" height="20">\
                        系統設定\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
                <br><br><br>\
                <ul class="jsfirst-level-area">\
                    <a href="javascript:void();" class="patNavMenu-list patNavMenu-list--active pb-10" title="系統設定">\
                        <img class="patNavMenu-icon" src="images/logout-icon.svg" alt="系統設定" width="20" height="20">\
                        系統登出\
                    </a>\
                    <div class="clear"></div>\
                </ul>\
            </section>\
        </nav>\
        <nav class="patBigNav">\
            <a href="javascript:void(0);" class="elenote patBigNav-note jsNoteBt">\
                <span class="elenote-count patBigNav-note--count">15</span>\
                <img src="images/note-icon.svg" alt="打開系統通知區" width="30px" height="30px">\
            </a>\
            <div class="patBigNav-link">\
                <div class="patBigNav-bt patBigNav-bt--pagein" data-tooltip="系統首頁" data-position="right center">\
                    <a href="overview.html">\
                        <img src="images/home-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="案件管理" data-position="right center">\
                    <a href="projectli.html">\
                        <img src="images/project-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="顧問管理" data-position="right center">\
                    <a href="consultantli.html">\
                        <img src="images/consultant-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="行事曆" data-position="right center">\
                    <a href="calendar.html">\
                        <img src="images/calendar-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="使用者管理" data-position="right center">\
                    <a href="usersli.html">\
                        <img src="images/user-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="日誌" data-position="right center">\
                    <a href="logs.html">\
                        <img src="images/log-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
                <div class="patBigNav-bt" data-tooltip="系統設定" data-position="right center">\
                    <a href="setting.html">\
                        <img src="images/setting-icon.svg" alt="home" class="patBigNav-bt--icon">\
                    </a>\
                </div>\
            </div>\
            <a href="javascript:void(0);" class="patBigNav-logout" title="登出" data-tooltip="登出" data-position="top center">\
                <img src="images/logout-icon.svg" alt="登出icon" width="30" height="30">\
            </a>\
            <br><br><br><br>\
        </nav>\
        <nav class="patSmlNavNote jspatSmlNavNote">\
            <div class="patSmlNavNote jspatSmlNavNote">\
                <div class="patSmlNavNote-closeBt jsNavSmlNote-bt--close">\
                    <img src="images/pop-delete-icon.svg" alt="close button" width="30" height="30">\
                </div>\
                <section class="patSmlNavNote-note pt-30 pb-20 plr-30">\
                    <p class="patSmlNavNote-note--list">\
                        加入共同協作案件：\
                    </p>\
                    <a href="projectin.html" class="patSmlNavNote-note--list--link">\
                        臺灣基隆地方法院 107 年訴字第 536號刑事判決\
                    </a>\
                    <h6 class="patSmlNavNote-note--time pt-15">\
                        <img class="patSmlNavNote-note--time--icon" src="images/clock-icon.svg" alt="clock icon">\
                        2020/07/16　15:36:42\
                    </h6>\
                </section>\
            </div>\
        </nav>\
        <div class="patSmlNavopen_bg jsNavSml-openbg"></div>\
    ');

    
});