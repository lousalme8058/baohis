<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">找拌麵</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">找</span>
						<span class="elepageTit--word">拌</span>
						<span class="elepageTit--word">麵</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<div class="paProliArea">
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<!-- 產品 -->
						<article class="paProli">
							<h4 class="paProli-spec">四入一袋</h4>
							<a href="productsin.php" class="paProli-img mb-30">
							<img src="images/pro01.png" alt="產品照片">
							</a>
							<a href="productsin.php" class="paProli-tit">
								江湖牛肉拌麵江湖牛肉拌麵
							</a>
							<div class="paProli-price">
								<span class="paProli-price--ori">$500</span> 
								<h6 class="paProli-price--act">$400</h6>
							</div>
							<div class="clear"></div>
							<a href="javascript:void(0);" class="btnRedBt mt-20">
								直接購買
							</a>
							<div class="clear"></div>
						</article>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			
			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     