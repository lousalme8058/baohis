<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">影音專區</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">影</span>
						<span class="elepageTit--word">音</span>
						<span class="elepageTit--word">專</span>
						<span class="elepageTit--word">區</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					

					<!-- 影音專區 -->
					<!-- 一頁5個 -->
					<div class="pavideoliArea">
						<!-- 一個影音消息 -->
						<a href="videoin.php" class="pavideolist">
							<div class="pavideolist-label">
								<h1 class="pavideolist-label--red">2020</h1>
								<h1 class="pavideolist-label--black">07.04</h1>
							</div>
							<div class="panewslistVideoImg">
								<!-- 建議尺寸980*515 -->
								<img src="images/videoimg.png" alt="img" width="100%" height="auto">
							</div>
							<h2 class="pavideolistTit mt-30">影音專區標題影音專區標題</h2>
						</a>
						<!-- 一個影音消息 -->
						<a href="videoin.php" class="pavideolist">
							<div class="pavideolist-label">
								<h1 class="pavideolist-label--red">2020</h1>
								<h1 class="pavideolist-label--black">07.04</h1>
							</div>
							<div class="panewslistVideoImg">
								<!-- 建議尺寸980*515 -->
								<img src="images/videoimg.png" alt="img" width="100%" height="auto">
							</div>
							<h2 class="pavideolistTit mt-30">影音專區標題影音專區標題</h2>
						</a>
					</div>
				</div>
				<div class="clear"></div>
				<!-- 跳轉頁面區 -->
				<section class="elePage mb-50">
					<a href="javascript:void(0);" class="elePage-arrowBt elePage-arrowBt--back">
						<img src="images/back-icon.png" alt="">
					</a>
					<span class="elePage-conut">1 OF 213</span>
					<a href="javascript:void(0);" class="elePage-arrowBt elePage-arrowBt--next">
						<img src="images/next-icon.png" alt="">
					</a>
				</section>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     