<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">影音專區</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">影</span>
						<span class="elepageTit--word">音</span>
						<span class="elepageTit--word">專</span>
						<span class="elepageTit--word">區</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					
					<!-- 影音專區 -->
					<div class="pavideoliArea">
						<div class="pavideolist">
							<div class="pavideolist-label">
								<h1 class="pavideolist-label--red">2020</h1>
								<h1 class="pavideolist-label--black">07.04</h1>
							</div>
							<div class="panewslistVideoImg">
								<div class="panewslistVideoBk">
									<!-- 影片放這 -->
									<iframe width="560" height="315" src="https://www.youtube.com/embed/B0crT8veKbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
									<!-- <iframe src="https://www.youtube.com/embed/B0crT8veKbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
									<!-- <iframe src="https://www.youtube.com/embed/HEquod00t-g" frameborder="0" allowfullscreen></iframe>		 -->
								</div>
							</div>
							<h2 class="pavideolistTit mt-30">影音專區標題影音專區標題</h2>
							<iframe width="560" height="315" src="https://www.youtube.com/embed/B0crT8veKbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<!-- 文邊區 -->
						<article class="panewslistVideText">
							<!-- 文編放這 -->
							<h4>文編區</h4>
						</article>
						<!-- 分享區 -->
						<article class="panewslistVideShare">
							<!-- 社群分享插件放這 -->
							
						</article>
					</div>

					<div class="modBtarea mt-40">
						<a href="videoli.php" class="btnRedBt modBtarea-backBt">回影音專區列表</a>
					</div>
				</div>

			
		
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     