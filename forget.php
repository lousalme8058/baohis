<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">忘記密碼</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">忘</span>
						<span class="elepageTit--word">記</span>
						<span class="elepageTit--word">密</span>
						<span class="elepageTit--word">碼</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width paCartFinHeight">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<div class="paForgetArea">
						<article class="paForget">

							<div class="ui form">
								<div class="field fidArea fidArea--nogutter">
									<label for="電子郵件">電子郵件<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="電子郵件">
								</div>
							</div>
							<a href="javascript:(0);" class="btnRedBt mb-10">查詢密碼</a>
							<a href="login.php" class="btnWhiteBt mb-10">回登入會員頁</a>
							<br>
						</article>
				
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>


			<!-- 元素動畫 -->
			<img src="images/asset-34.png" alt="img" class="elepageAniArea02 wow">
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     