<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">加入會員</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">加</span>
						<span class="elepageTit--word">入</span>
						<span class="elepageTit--word">會</span>
						<span class="elepageTit--word">員</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width paCartFinHeight">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<div class="paSignupArea">
						<!-- 頁面切換按鈕 -->
						<article class="eleTabArea">
							<!-- 頁面懸停時加eleTab--in -->
							<a href="login.php" class="eleTab">
								會員登入
								<img src="images/next-icon.png" width="70" height="auto" alt="">
							</a>
							<a href="sign-up.php" class="eleTab eleTab--in">
								加入會員
								<img src="images/next-icon.png" width="70" height="auto" alt="">
							</a>
						</article>

						<article class="paSignup">
							<div class="ui form">
								<div class="field fidArea fidArea--50">
									<label for="電子郵件">電子郵件<span class="fieverti">＊</span></label>
									<input type="email" name="" id="" placeholder="電子郵件">
								</div>
								<div class="field fidArea fidArea--50">
									<label for="姓名">姓名<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="姓名">
								</div>
								<div class="field fidArea fidArea--50">
									<label for="密碼">密碼<span class="fieverti">＊</span></label>
									<input type="password" name="" id="" placeholder="密碼">
								</div>
								<div class="field fidArea fidArea--50">
									<label for="再次輸入密碼">再次輸入密碼<span class="fieverti">＊</span></label>
									<input type="password" name="" id="" placeholder="再次輸入密碼">
								</div>
								<div class="field fidArea fidArea--50">
									<label for="連絡電話">連絡電話<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="連絡電話">
								</div>
								<div class="field fidArea fidArea--50">
									<label for="手機">手機<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="手機">
								</div>
								<!-- <div class="field fidArea fidArea--50">
									<label for="寄送地址">寄送地址<span class="fieverti">＊</span></label>
									<select class="ui dropdown">
										<option value="">輸入縣／市</option>
										<option value="台北市">台北市</option>
									</select>
								</div>
								<div class="field fidArea fidArea--50">
									<select class="ui dropdown">
										<option value="">輸入區域</option>
										<option value="中和區">中和區</option>
									</select>
								</div>
								<div class="field fidArea fidArea--50">
									<input type="text" name="" id="" placeholder="輸入地址">
								</div>

								<div class="clear"></div> -->

								<div class="field fidArea">
									<label for="寄送地址">寄送地址<span class="fieverti">＊</span></label>
									<div class="paSignup-add mb-10">
										<select class="ui dropdown">
											<option value="">輸入縣／市</option>
											<option value="台北市">台北市</option>
										</select>
									</div>
									<div class="paSignup-add mb-10">
										<select class="ui dropdown">
											<option value="">輸入區域</option>
											<option value="中和區">中和區</option>
										</select>
									</div>
									<div class="paSignup-add">
										<input type="text" name="" id="" placeholder="輸入地址">
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</article>
					</div>

					<div class="modBtarea mt-30">
						<a href="javascript:void(0);" class="btnBlackBt modBtarea-nextBt">
							<img src="images/next-icon.png" width="70" height="auto" alt="">
							註冊
						</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

			<!-- 元素動畫 -->
			<!-- <img src="images/asset-34.png" alt="img" class="elepageAniArea02 wow"> -->
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     