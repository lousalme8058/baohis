<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">購物車</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">購</span>
						<span class="elepageTit--word">物</span>
						<span class="elepageTit--word">車</span>
					</h1>
					<div class="elecartStepTitArea">
						<h4 class="elecartStepTit elecartStepTit--inpage">確認數量</h4>
						<h4 class="elecartStepTit elecartStepTit--phoneHide">填寫資料</h4>
						<h4 class="elecartStepTit elecartStepTit--phoneHide">選擇付款方式</h4>
					</div>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>

			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					
					<!-- 購物車列表 -->
					<div class="paCartArea">
						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
								<div class="paCartProli-select">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter ptb-0">
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
								</div>
								<div class="paCartProli-delbt">
									<a href="javascript:void(0);" class="delBt">
										<img src="images/del-icon.png" alt="icon">
									</a>
								</div>
							</div>
							<div class="paCartProli-secPro">
								<h6 class="paCartProli-secPro--secTit mt-20 plr-30">選擇產品組合</h6>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit">
										江湖牛肉拌麵
									</h3>
									<div class="paCartProli-secProli--select">
										<div class="ui form">
											<div class="field fidArea fidArea--nogutter ptb-0">
												<select class="ui dropdown">
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
												</select>
											</div>
										</div>
									</div>
								</article>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit">
										江湖牛肉拌麵
									</h3>
									<div class="paCartProli-secProli--select">
										<div class="ui form">
											<div class="field fidArea fidArea--nogutter ptb-0">
												<select class="ui dropdown">
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
												</select>
											</div>
										</div>
									</div>
								</article>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit">
										江湖牛肉拌麵江湖牛肉拌麵江湖牛肉拌麵
									</h3>
									<div class="paCartProli-secProli--select">
										<div class="ui form">
											<div class="field fidArea fidArea--nogutter ptb-0">
												<select class="ui dropdown">
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
												</select>
											</div>
										</div>
									</div>
								</article>
							</div>
						</article>

						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
								<div class="paCartProli-select">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter ptb-0">
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
								</div>
								<div class="paCartProli-delbt">
									<a href="javascript:void(0);" class="delBt">
										<img src="images/del-icon.png" alt="icon">
									</a>
								</div>
							</div>
							<div class="paCartProli-secPro">
								<h6 class="paCartProli-secPro--secTit mt-20 plr-30">選擇產品組合</h6>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit">
										江湖牛肉拌麵
									</h3>
									<div class="paCartProli-secProli--select">
										<div class="ui form">
											<div class="field fidArea fidArea--nogutter ptb-0">
												<select class="ui dropdown">
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
												</select>
											</div>
										</div>
									</div>
								</article>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit">
										江湖牛肉拌麵江湖牛肉拌麵江湖牛肉拌麵
									</h3>
									<div class="paCartProli-secProli--select">
										<div class="ui form">
											<div class="field fidArea fidArea--nogutter ptb-0">
												<select class="ui dropdown">
													<option value="01">01</option>
													<option value="02">02</option>
													<option value="03">03</option>
													<option value="04">04</option>
													<option value="05">05</option>
												</select>
											</div>
										</div>
									</div>
								</article>
							</div>
						</article>

						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
								<div class="paCartProli-select">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter ptb-0">
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
								</div>
								<div class="paCartProli-delbt">
									<a href="javascript:void(0);" class="delBt">
										<img src="images/del-icon.png" alt="icon">
									</a>
								</div>
							</div>
						</article>
						
						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
								<div class="paCartProli-select">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter ptb-0">
											<select class="ui dropdown">
												<option value="01">01</option>
												<option value="02">02</option>
												<option value="03">03</option>
												<option value="04">04</option>
												<option value="05">05</option>
											</select>
										</div>
									</div>
								</div>
								<div class="paCartProli-delbt">
									<a href="javascript:void(0);" class="delBt">
										<img src="images/del-icon.png" alt="icon">
									</a>
								</div>
							</div>
						</article>
					</div>

					<div class="modBtarea">
						<a href="cart02.php" class="btnBlackBt modBtarea-nextBt mb-10">
							<img src="images/next-icon.png" width="70" height="auto" alt="">
							填寫訂單資料
						</a>
						<a href="javascript:void(0);" class="btnWhiteBt modBtarea-backBt mb-10">繼續購物</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     