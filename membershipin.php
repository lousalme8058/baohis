<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">會員專區</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">會</span>
						<span class="elepageTit--word">員</span>
						<span class="elepageTit--word">專</span>
						<span class="elepageTit--word">區</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="membership.php" class="eleTab eleTab--in">
							訂單紀錄
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
						<a href="membership-profile.php" class="eleTab">
							會員資料
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
					</article>

					<!-- 訂單資料區 -->
					<div class="papmemberArea">
						<!-- 訂單列表 -->
						<article class="papmemberList mb-30">
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單日期</h6>
								<h4 class="papmemberContent-text">2020.05.24</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單編號</h6>
								<h4 class="papmemberContent-text">202005240001</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單金額</h6>
								<h4 class="papmemberContent-text">$540</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單狀態</h6>
								<h4 class="papmemberContent-text">已出貨</h4>
							</div>
						</article>

						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea" style="margin-right: 0px;">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
							</div>
							<div class="paCartProli-secPro">
								<h6 class="paCartProli-secPro--secTit mt-20 plr-30">產品組合</h6>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit" style="margin-right: 0px;">
										江湖牛肉拌麵 　＊１組
									</h3>
								</article>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit" style="margin-right: 0px;">
										江湖牛肉拌麵 　＊１組
									</h3>
								</article>
								<!-- 產品組合 -->
								<article class="paCartProli-secProli">
									<img src="images/pro01.png" alt="產品照片" class="paCartProli-secProli--secProliimg">
									<h3 class="paCartProli-secProli--secProliTit" style="margin-right: 0px;">
										江湖牛肉拌麵 　＊１組
									</h3>
								</article>
							</div>
						</article>

						<!-- 一個產品 -->
						<article class="paCartProli mb-30">
							<div class="paCartProli-mainPro">
								<img src="images/pro01.png" alt="產品照片" class="paCartProli-proimg">
								<h3 class="paCartProli-textArea" style="margin-right: 0px;">
									<span class="paCartProli-textArea--tit">任選2袋免運組　＊１組</span>
									<span class="paCartProli-textArea--price">$420</span>
								</h3>
							</div>
						</article>

						<article class="papmeminCount floatLeft">
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">姓名</span>
								<span class="eleorderLi-content">多拉A告</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">電話</span>
								<span class="eleorderLi-content">04-5355-7432</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">手機</span>
								<span class="eleorderLi-content typo-danger">0919-369-323</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">寄送地址</span>
								<span class="eleorderLi-content typo-danger">台中市西屯區工業區一路98巷25號5F-3</span>
							</h4>
							<h4 class="eleorderLi eleorderLi--emphasis">
								<span class="eleorderLi-tit">備註</span>
								<span class="eleorderLi-content typo-danger">請快點送來</span>
							</h4>
							<div class="clear"></div>
						</article>

						<article class="papmeminCount floatRight">
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">產品總金額</span>
								<span class="eleorderLi-content">$540</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">運費</span>
								<span class="eleorderLi-content">$60</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">第一次消費折扣</span>
								<span class="eleorderLi-content typo-danger">-$60</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">輸入折扣碼</span>
								<span class="eleorderLi-content typo-danger">-$60</span>
							</h4>
							<h4 class="eleorderLi eleorderLi--emphasis">
								<span class="eleorderLi-tit">總計</span>
								<span class="eleorderLi-content typo-danger">$480</span>
							</h4>
							<div class="clear"></div>
						</article>

						<article class="papmeminCount floatRight">
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">付款方式</span>
								<span class="eleorderLi-content"> ATM 轉帳</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">轉帳帳號</span>
								<span class="eleorderLi-content">
									中國信託銀行-嘉義分行<br />
									銀行代號：822-0082<br />
									帳號：0825-4081-4460<br />
									戶名：寶璽事業有限公司
								</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">轉帳後5碼
									<span class="fieverti">
										<br />
										轉帳後，輸入匯款帳號後五碼
									</span>	
								</span>
								<span class="eleorderLi-content">
									<div class="ui form">
										<div class="field fidArea fidArea--nogutter ptb-0">
											<input type="text" name="" id="" placeholder="輸入轉帳後5碼">
										</div>
										<div class="modBtarea mt-10 plr-0">
											<a href="membership.php" class="btnRedBt">確認送出</a>
										</div>
									</div>
								</span>
							</h4>
							<div class="clear"></div>
						</article>
						<div class="clear"></div>
					</div>
					
					<div class="modBtarea mt-40">
						<a href="membership.php" class="btnBlackBt modBtarea-backBt">回訂單列表</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     