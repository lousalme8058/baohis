<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">常見問題</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">常</span>
						<span class="elepageTit--word">見</span>
						<span class="elepageTit--word">問</span>
						<span class="elepageTit--word">題</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>


			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 產品介紹區 -->
					<div class="paQaArea">

						<!-- 一個問題 -->
						<article class="paQalist">
							<h3 class="paQa-q">
								<span class="paQa-label--red">Q</span>
								<span class="paQa-tit">在結帳的時候無法點選結帳按鈕或是點選後無法結帳？</span>
							</h3>
							<h3 class="paQa-a">
								<span class="paQa-label--black">A</span>
								<span class="paQa-tit">有些公司會管制上網內容，可以嘗試用其他非公司網路訂購或是使用手機上網（斷開公司WIFI）訂購。若還有其它問題，請洽客服人員。</span>
							</h3>
						</article>
						<!-- 一個問題 -->
						<article class="paQalist">
							<h3 class="paQa-q">
								<span class="paQa-label--red">Q</span>
								<span class="paQa-tit">在結帳的時候無法點選結帳按鈕或是點選後無法結帳？</span>
							</h3>
							<h3 class="paQa-a">
								<span class="paQa-label--black">A</span>
								<span class="paQa-tit">有些公司會管制上網內容，可以嘗試用其他非公司網路訂購或是使用手機上網（斷開公司WIFI）訂購。若還有其它問題，請洽客服人員。</span>
							</h3>
						</article>
						<!-- 一個問題 -->
						<article class="paQalist">
							<h3 class="paQa-q">
								<span class="paQa-label--red">Q</span>
								<span class="paQa-tit">在結帳的時候無法點選結帳按鈕或是點選後無法結帳？</span>
							</h3>
							<h3 class="paQa-a">
								<span class="paQa-label--black">A</span>
								<span class="paQa-tit">有些公司會管制上網內容，可以嘗試用其他非公司網路訂購或是使用手機上網（斷開公司WIFI）訂購。若還有其它問題，請洽客服人員。</span>
							</h3>
						</article>
						<!-- 一個問題 -->
						<article class="paQalist">
							<h3 class="paQa-q">
								<span class="paQa-label--red">Q</span>
								<span class="paQa-tit">在結帳的時候無法點選結帳按鈕或是點選後無法結帳？</span>
							</h3>
							<h3 class="paQa-a">
								<span class="paQa-label--black">A</span>
								<span class="paQa-tit">有些公司會管制上網內容，可以嘗試用其他非公司網路訂購或是使用手機上網（斷開公司WIFI）訂購。若還有其它問題，請洽客服人員。</span>
							</h3>
						</article>
						

						
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     