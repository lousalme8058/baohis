<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">購物車</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">購</span>
						<span class="elepageTit--word">物</span>
						<span class="elepageTit--word">車</span>
					</h1>
					<div class="elecartStepTitArea">
						<h4 class="elecartStepTit elecartStepTit--phoneHide">確認數量</h4>
						<h4 class="elecartStepTit elecartStepTit--inpage">填寫資料</h4>
						<h4 class="elecartStepTit elecartStepTit--phoneHide">選擇付款方式</h4>
					</div>
				</article>
				
				<!-- 元素動畫 -->
				<img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow">
			</aside>

			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<!-- 訂單資料區 -->
					<div class="paCartOrderArea">
						<!-- 訂單金額計算 -->
						<article class="paCartOrder-leftArea">
							<h3 class="mb-30">訂單金額計算</h3>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">產品總金額</span>
								<span class="eleorderLi-content">$540</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">運費</span>
								<span class="eleorderLi-content">$60</span>
							</h4>
							<h4 class="eleorderLi">
								<span class="eleorderLi-tit">第一次消費折扣</span>
								<span class="eleorderLi-content typo-danger">-$60</span>
							</h4>
							<h4 class="eleorderLi eleorderLi--field">
								<span class="eleorderLi-tit">輸入折扣碼</span>
								<span class="eleorderLi-content">
								<div class="ui form">
									<div class="field fidArea fidArea--nogutter ptb-0">
										<input type="text" name="" id="" placeholder="折扣碼">
									</div>
								</div>
								</span>
							</h4>
							<h4 class="eleorderLi eleorderLi--emphasis">
								<span class="eleorderLi-tit">總計</span>
								<span class="eleorderLi-content typo-danger">$480</span>
							</h4>
							<div class="clear"></div>
						</article>
						
						<!-- 填寫配送資訊 -->
						<article class="paCartOrder-rightArea">
							<h3 class="mb-30">填寫配送資訊</h3>
							<div class="ui form">
								<div class="field fidArea fidArea--nogutter">
									<label for="配送資訊">配送資訊<span class="fieverti"></span></label>
									<div class="ui checkbox">
										<input type="checkbox" tabindex="0" class="hidden">
										<label>同會員資料</label>
									</div>
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="姓名">姓名<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="姓名">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="手機">手機<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="手機">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="寄送地址">寄送地址<span class="fieverti">＊</span></label>
									<select class="ui dropdown mb-15">
										<option value="">輸入縣／市</option>
										<option value="台北市">台北市</option>
									</select>
									<select class="ui dropdown mb-15">
										<option value="">輸入區域</option>
										<option value="中和區">中和區</option>
									</select>
									<input type="text" name="" id="" placeholder="輸入地址">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="備註">備註<span class="fieverti">＊</span></label>
									<textarea name="" id="" cols="30" rows="10" placeholder="備註"></textarea>
								</div>
							</div>
							<div class="clear"></div>
						</article>
						<div class="clear"></div>
					</div>
					

					<div class="modBtarea mt-30">
						<a href="cart03.php" class="btnBlackBt modBtarea-nextBt mb-10">
							<img src="images/next-icon.png" width="70" height="auto" alt="">
							選擇付款方式
						</a>
						<a href="cart01.php" class="btnWhiteBt modBtarea-backBt mb-10">回購物車</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     