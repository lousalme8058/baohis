<!-- 小視口及置頂導覽列 -->
<header class="patSmlHeader"> 
	<a href="javascript:void(0);" class="patSmlHeader-navbt jsNavSml-bt">
		<img src="images/hambuger-icon.png" alt="打開功能列表" width="30px" height="30px" class="patSmlHeader-navbt--img">
	</a>
	<a href="cart01.php" class="patSmlHeader-cartbt">
		<span class="patSmlHeader-cartbt--count">12</span>
		<img src="images/cart-icon-white.png" alt="前往購物車" width="30px" height="30px" class="patSmlHeader-cartbt--img">
	</a>
</header>

<nav class="patSmlNav jsNavSmall">
	<div class="patSmlNav-closeBt jsNavSml-bt--close">
		<img src="images/pop-delete-icon.png" alt="close button" width="30" height="30" class="patSmlNav-closeBt--img">
	</div>
	<section class="patNavMenu">
		<div class="ptb-40 plr-30">
			<ul class="jsfirst-level-area">
				<a href="index.php" class="patNavMenu-list patNavMenu-list--active pb-10" title="回首頁">
					<img src="images/red-dot-icon.png" alt="icon" class="mr-5">
					回首頁
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="productsli.php" class="patNavMenu-list pb-10" title="購買產品">
					<img src="images/red-dot-icon.png" alt="icon" class="mr-5">
					購買產品
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="freeshippingli.php" class="patNavMenu-list pb-10" title="免運組合">
					<img src="images/red-dot-icon.png" alt="icon" class="mr-5">
					免運組合
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="qa.php" class="patNavMenu-list pb-10" title="常見問題">
					<img src="images/red-dot-icon.png" alt="icon" class="mr-5">
					常見問題
				</a>
				<div class="clear"></div>
			</ul>
		</div>
		<div class="patNavMenu-viceLinkArea ptb-40 plr-30">
			<ul class="jsfirst-level-area">
				<a href="about.php" class="patNavMenu-list pb-10" title="關於我們">
					關於我們
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="newsli.php" class="patNavMenu-list pb-10" title="最新消息">
					最新消息
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="membership.php" class="patNavMenu-list pb-10" title="會員專區">
					會員專區
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="videoli.php" class="patNavMenu-list pb-10" title="會員專區">
					影音專區
				</a>
				<div class="clear"></div>
			</ul>
			<ul class="jsfirst-level-area">
				<a href="javascript:void();" class="patNavMenu-list pb-10" title="系統登出">
					系統登出
				</a>
				<div class="clear"></div>
			</ul>
		</div>
	</section>
</nav>
