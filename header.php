<div class="patBigNav--bg">
	<div class="max_width">
		<nav class="patBigNav">
			<!-- 確實霸道logo -->
			<a href="index.php" title="回首頁">
				<img src="images/big-logo.png" alt="logo" class="wow patBigNav-logoArea--logo">
				<img src="images/logo-bg.png" alt="logo bg" class="wow patBigNav-logoArea--ani">
			</a>
			<!-- 主要功能 -->
			<article class="patBigNav-mainLinkArea pr-80">
				<a href="productsli.php" class="patBigNav-Link" title="購買產品">
					<h6 class="patBigNav-Link--en">Buy Products</h6>
					<h4 class="patBigNav-Link--ch">
						<img src="images/red-dot-icon.png" alt="icon" class="patBigNav-Link--ch--icon">
						購買產品
					</h4>
				</a>
				<a href="freeshippingli.php" class="patBigNav-Link" title="免運組合">
					<h6 class="patBigNav-Link--en">Free Shipping</h6>
					<h4 class="patBigNav-Link--ch">
						<img src="images/red-dot-icon.png" alt="icon" class="patBigNav-Link--ch--icon">
						免運組合
					</h4>
				</a>
				<a href="qa.php" class="patBigNav-Link" title="常見問題">
					<h6 class="patBigNav-Link--en">Q & A</h6>
					<h4 class="patBigNav-Link--ch">
						<img src="images/red-dot-icon.png" alt="icon" class="patBigNav-Link--ch--icon">
						常見問題
					</h4>
				</a>
			</article>
			<article class="patBigNav-secondLinkArea pr-80">
				<a href="cart01.php" class="patBigNav-Link--viceLink" title="購物車">
					<h4 class="patBigNav-Link--ch">
						購物車　
					</h4>
				</a>
				<a href="membership.php" class="patBigNav-Link--viceLink" title="會員專區">
					<h4 class="patBigNav-Link--ch">
						會員專區
					</h4>
				</a>
				<!-- <a href="javascript:void(0);" class="patBigNav-Link--viceLink" title="登出">
					<h4 class="patBigNav-Link--ch">
						會員登出
					</h4>
				</a> -->
			</article>
			<article class="patBigNav-thirdLinkArea ">
				<a href="about.php" class="patBigNav-Link--viceLink" title="關於我們">
					<h4 class="patBigNav-Link--ch">
						關於我們
					</h4>
				</a>
				<a href="newsli.php" class="patBigNav-Link--viceLink" title="最新消息">
					<h4 class="patBigNav-Link--ch">
						最新消息
					</h4>
				</a>
				<a href="videoli.php" class="patBigNav-Link--viceLink" title="影音專區">
					<h4 class="patBigNav-Link--ch">
						影音專區
					</h4>
				</a>
			</article>
		</nav>
	</div>
	<div class="clear"></div>
</div>