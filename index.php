<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        nav: false,
        loop: true,
        dots: false,
        responsive: {
        320: {
            items: 1
        },
        }
    });
});
</script>

<script>
$(document).ready(function() {
    //出現錨點
    $(".jsindthumbArea").css("display","none");
	$(window).scroll(function(){
		var bannerHeight = $(".indBanner").height();
		// console.log("banner高度:"+bannerHeight);
		var showBtH = bannerHeight - 150;
		// console.log(bannerHeight + "- 150 = "+showBtH);
		// 該元素只有適口1856px出現,如果抓不到,判斷式不會觸發
		var eleHeight = $(".indele-pro05").offset().top;
		var winHeight = $(window).height();
		// console.log(eleHeight);
		// console.log(winHeight);
		var hidBtH = eleHeight - winHeight + 100;
		// console.log("卷軸關閉高度"+hidBtH);
		// console.log($(window).scrollTop());
		if($(window).scrollTop()>= showBtH && $(window).scrollTop()<= hidBtH){
			$(".jsindthumbArea").css("display","block");
			TweenMax.staggerTo(".jsthumb", 0, {transform: "translateY(0px)", opacity:1, delay:0}, 0.1);
		}else{
			// console.log("關閉");
			TweenMax.to(".jsthumb", 0.5, {transform: "translateY(-50px)", opacity:0, delay:0}, 0);
		}
    });

    //錨點定位
	$(".indthumb01").on('click',function(){
		var noodle01Height = $("#jsnoodle01").offset().top + 90;
		// console.log(noodle01Height);
		//window.scrollTo為javascript原生事件
		TweenMax.to(window, 1, {scrollTo: {y: noodle01Height}, ease: Power2.easeOut});
	});
	$(".indthumb02").on('click',function(){
		var noodle02Height = $("#jsnoodle02").offset().top -110 ;
		// console.log(noodle02Height);
		TweenMax.to(window, 1, {scrollTo: {y: noodle02Height}, ease: Power2.easeOut});
	});
	$(".indthumb03").on('click',function(){
		var noodle03Height = $("#jsnoodle03").offset().top ;
		// console.log(noodle03Height);
		TweenMax.to(window, 1, {scrollTo: {y: noodle03Height}, ease: Power2.easeOut});
	});
	$(".indthumb04").on('click',function(){
		var noodle04Height = $("#jsnoodle04").offset().top +80 ;
		// console.log(noodle04Height);
		TweenMax.to(window, 1, {scrollTo: {y: noodle04Height}, ease: Power2.easeOut});
	});

})
// window.onload會等網頁的全部內容，包括圖片，CSS及<iframe>等外部內容載入後才會觸發，但$(document).ready()在Document Object Model (DOM) 載入後就會觸發，所以順序上$(document).ready()會比window.onload先執行。
//window.onload是JavaScript的原生事件，而$(document).ready()是jQuery的事件（其實是透過監聽JavaScript的DOMContentLoaded事件來實現）。
$(window).on('load',function(){
    $(".js-indexAniBg").addClass("indloadingHide");
    $(".js-indexAni").addClass("indloadingAni");
    // alert("網頁全部元素載完了");
});
</script>
</head>
<body>
    <!-- <div class="loading_filter"> -->
    <!-- loading動畫 -->
    <div class="js-indexAniBg indloadingHideArea"></div>
    <div class="js-indexAni"></div>
	
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patWrapper">
            
            <!-- 大視口導覽列 -->       
            <?php require('header.php') ?>

            <!-- <img src="images/banner01-landscape.png" alt="預設輪播圖">  -->
            <div class="clear"></div>


            <!-- 首頁輪播區 -->
            <article class="indBanner owl-carousel">
                <a href="productsli.php">
                    <picture>
                        <source srcset="images/banner01-portrait.png" media="(max-width: 48em)" width="100%" height="auto">
                        <source srcset="images/banner01-landscape.png" media="(min-width: 48em)" width="100%" height="auto">
                        <img src="images/banner01-landscape.png" alt="預設輪播圖" width="100%" height="auto">    
                    </picture>
                </a>
                <a href="productsli.php">
                    <picture>
                        <source srcset="images/banner01-portrait.png" media="(max-width: 48em)" width="100%" height="auto">
                        <source srcset="images/banner01-landscape.png" media="(min-width: 48em)" width="100%" height="auto">
                        <img src="images/banner01-landscape.png" alt="預設輪播圖" width="100%" height="auto">    
                    </picture>
                </a>
            </article>

            <!-- <a href="productsli.php">
                <picture>
                    直式，手機/ipad用 
                    <source srcset="images/banner01-portrait.png" media="(max-width: 48em)">
                    橫式，電腦版用
                    <source srcset="images/banner01-landscape.png" media="(min-width: 48em)">
                     預設輪播圖橫式 
                    <img src="images/banner01-landscape.png" alt="預設輪播圖">    
                </picture>
            </a> -->

            <!-- 廣告文編區 -->
            <div class="max_width">
                <article class="indAdArea">
                    <img src="images/adBanner01.jpg" alt="廣告圖" style="width:100%;height: auto;">
                </article>
            </div>

            <!-- 元素動畫 -->
            <div class="indProAni">
                <img src="images/asset-01.png" class="wow indele-pro01" alt="img">
                <div class="wow indele-pro02--horizontal">
                    <img src="images/asset-02.png" class="wow indele-pro02--vertical" alt="img">
                </div>
                <img src="images/asset-04.png" class="wow indele-pro03" alt="img">
                <img src="images/asset-05.png" class="wow indele-pro04" alt="img">
                <img src="images/asset-06.png" class="wow indele-pro05" alt="img">
            </div>

            <!-- 拌麵介紹跳轉錨點 -->
            <div class="indthumbArea jsindthumbArea">
                <a href="javascript:void(0);" class="indthumb01 jsthumb">
                    <h6 class="indthumb-tit">江湖牛肉</h6>
                    <img src="images/thumb-01.png" alt="江湖牛肉小圖">
                </a>
                <a href="javascript:void(0);" class="indthumb02 jsthumb">
                    <h6 class="indthumb-tit">初心 XO</h6>
                    <img src="images/thumb-02.png" alt="初心xo醬小圖">
                </a>
                <a href="javascript:void(0);" class="indthumb03 jsthumb">
                    <h6 class="indthumb-tit">初戀麻醬</h6>
                    <img src="images/thumb-03.png" alt="初戀麻醬小圖">
                </a>
                <a href="javascript:void(0);" class="indthumb04 jsthumb">
                    <h6 class="indthumb-tit">幸福椒麻</h6>
                    <img src="images/thumb-04.png" alt="幸福椒麻小圖">
                </a>
            </div>

            <!-- 首頁產品區 -->
            <div class="max_width">
                <div class="indProArea">
                    <!-- 標題 -->
                    <div class="indPro-titArea">
                        <h1 class="indPro-titArea--cn">
                            <span class="indPro-titArea--cn--word">找</span>
                            <span class="indPro-titArea--cn--word">拌</span>
                            <span class="indPro-titArea--cn--word">麵</span>
                        </h1>
                        <h6 class="indPro-titArea--en mt-10">Buy Noodles</h6>
                    </div>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article> 
                </div>

                <div class="clear"></div>

                <!-- 免運組合區 -->
                <div class="indProArea">
                    <!-- 標題 -->
                    <div class="indPro-titArea">
                        <h1 class="indPro-titArea--cn">
                            <span class="indPro-titArea--cn--word">免</span>
                            <span class="indPro-titArea--cn--word">運</span>
                            <span class="indPro-titArea--cn--word">組</span>
                            <span class="indPro-titArea--cn--word">合</span>
                        </h1>
                        <h6 class="indPro-titArea--en mt-10">Free shipping</h6>
                    </div>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article>
                    <!-- 產品 -->
                    <article class="indPro mb-40">
                        <h4 class="indPro-spec">四入一袋</h4>
                        <a href="productsin.php" class="indPro-img mb-30">
                            <img src="images/pro01.png" alt="產品照片">
                        </a>
                        <a href="productsin.php" class="indPro-tit">
                            江湖牛肉拌麵江湖牛肉拌麵
                        </a>
                        <div class="indPro-price">
                            <span class="indPro-price--ori">$500</span> 
                            <h6 class="indPro-price--act">$400</h6>
                        </div>
                        <div class="clear"></div>
                        <a href="javascript:void(0);" class="btnRedBt mt-20">
                            直接購買
                        </a>
                        <div class="clear"></div>
                    </article> 
                </div>
            </div>

            <div class="clear"></div>
            
            <!-- 產品介紹區 -->
            <div class="indintroArea">
                <!-- <img src="images/intro-top.png" alt="bg" class="indintroTopBg"> -->
                <div class="max_width">
                    <!-- 藤椒文案 -->
                    <article class="indintro-section01">
                        <section class="indsecond01-textArea">
                            <h3 class="indsecond01-introText mb-40">
                                原味重現川菜經典，堅持把最好的品質給最好的你！ <br />
                                確實霸道專程從產地進口藤椒油，<br />原味重現川味經典的香「麻」口味！
                            </h3>
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--blackBg">香</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--whiteBg">麻</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--redBg">辣</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                        </section>
                        <img src="images/asset-32.png" alt="藤椒圖" class="indsecond01-img wow">
                        <div class="clear"></div>
                    </article>
                </div>

                <!-- 江湖牛肉拌麵區 -->
                <article id="jsnoodle01" class="indintro-section02">
                    <div class="indintro02-tit indintroTitArea">
                        <!-- <h1 class="indNoodleTit wow">江湖</h1> -->
                        <h1 class="indNoodleTit wow">
                            <span class="indNoodleTit--word">江</span>
                            <span class="indNoodleTit--word">湖</span>
                        </h1>
                        <h4 class="indNoodleSubTit wow">
                            <span class="indNoodleSubTit--word">牛</span>
                            <span class="indNoodleSubTit--word">肉</span>
                            <span class="indNoodleSubTit--word">拌</span>
                            <span class="indNoodleSubTit--word">麵</span>
                        </h4>
                    </div>
                    <div class="indintro02-noodleImgArea">
                        <img src="images/asset-20-noodle01.png" alt="煙霧" class="indintro02--smokeImg wow">
                        <img src="images/asset-21-noodle01.png" alt="江湖牛肉拌麵形象圖" class="indintro02--noodleBgImg wow">
                    </div>
                    <a href="productsin.php" class="indintro02-intro indtextBlack wow">
                        <h4 class="indtextBlack-inrto">帶某子鬥陣</h4>
                        <h4 class="indtextBlack-inrto">頭家兩碗牛肉拌麵，加辣！</h4>
                        <div class="indtextBlack-text">
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--redBg">辣</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <h4 class="indtextBlack-text--Style">
                                一上市就大受好評的牛肉口味！調味以眷村外省口味做標準，牛肉麵的口味偏重，香辣刺激，就像觀看黑道江湖的電影一樣，令人印象深刻！
                             </h4>
                        </div>
                        <img class="indtextBlack-arrow wow" src="images/arrow-bt.png" alt="產品連結內頁" width="92" height="15">
                        <img src="images/asset-10.png" alt="辣椒" class="indintro02-ele04 wow">
                    </a>
                    <img src="images/asset-08.png" alt="葉子" class="indintro02-ele01 wow">
                    <img src="images/asset-09.png" alt="葉子" class="indintro02-ele02 wow">
                    <img src="images/asset-07.png" alt="葉子" class="indintro02-ele03 wow">
                    <div class="clear"></div>
                </article>
                
                <!-- 初心xo醬拌麵區 -->
                <article class="indintro-section03">
                    <div id="jsnoodle02" class="indintro03-tit indintroTitArea">
                        <!-- <h1 class="indNoodleTit wow">初心</h1> -->
                        <h1 class="indNoodleTit wow">
                            <span class="indNoodleTit--word">初</span>
                            <span class="indNoodleTit--word">心</span>
                        </h1>
                        <h4 class="indNoodleSubTit wow">
                            <span class="indNoodleSubTit--word indNoodleSubTit--word--vertical">XO</span>
                            <span class="indNoodleSubTit--word">醬</span>
                            <span class="indNoodleSubTit--word">拌</span>
                            <span class="indNoodleSubTit--word">麵</span>
                        </h4>
                    </div>
                    <div class="indintro03-noodleImgArea">
                        <img src="images/asset-26-noodle02.png" alt="煙霧" class="indintro03--smokeImg wow">
                        <img src="images/asset-27-noodle02.png" alt="初心xo醬拌麵形象圖" class="indintro03--noodleBgImg wow">
                    </div>
                    <div class="clear"></div>
                    <a href="productsin.php" class="indintro03-intro indtextBlack wow">
                        <h4 class="indtextBlack-inrto">嚴選的堅持、不變的初心、</h4>
                        <h4 class="indtextBlack-inrto">海的美味し！</h4>
                        <div class="indtextBlack-text">
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--redBg">辣</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <h4 class="indtextBlack-text--Style">
                                確實霸道拌麵系列的暢銷款！選用台灣產的丁香小魚乾、干貝、辣椒、大蒜⋯師傅全手工，文火慢炒的 XO醬，研磨而
                                成的醬料下去調味！小批量生產，確保產品新鮮度！
                             </h4>
                        </div>
                        <img class="indtextBlack-arrow wow" src="images/arrow-bt.png" alt="產品連結內頁" width="92" height="15">
                        <img src="images/asset-11.png" alt="辣椒" class="indintro03-ele01 wow">
                    </a>
                    <div class="clear"></div>
                </article>

                <!-- 初戀麻醬拌麵區 -->
                <article class="indintro-section04">
                    <div class="indintro04-tit indintroTitArea">
                        <!-- <h1 class="indNoodleTit wow">初戀</h1> -->
                        <h1 class="indNoodleTit wow">
                            <span class="indNoodleTit--word">初</span>
                            <span class="indNoodleTit--word">戀</span>
                        </h1>
                        <h4 class="indNoodleSubTit wow">
                            <span class="indNoodleSubTit--word">麻</span>
                            <span class="indNoodleSubTit--word">醬</span>
                            <span class="indNoodleSubTit--word">拌</span>
                            <span class="indNoodleSubTit--word">麵</span>
                        </h4>
                    </div>
                    <div id="jsnoodle03" class="indintro04-noodleImgArea">
                        <img src="images/asset-29-noodle03.png" alt="煙霧" class="indintro04--smokeImg wow">
                        <img src="images/asset-30-noodle03.png" alt="初戀麻醬拌麵形象圖" class="indintro04--noodleBgImg wow">
                        <img src="images/asset-12.png" alt="櫻花" class="indintro04-ele01 wow">
                        <img src="images/asset-13.png" alt="櫻花" class="indintro04-ele02 wow">
                        <img src="images/asset-14.png" alt="櫻花" class="indintro04-ele03 wow">
                    </div>
                    <a href="productsin.php" class="indintro04-intro indtextBlack wow">
                        <h4 class="indtextBlack-inrto">尋找記憶中，最美的時光！</h4>
                        <h4 class="indtextBlack-inrto">一個不曾被遺忘的味道</h4>
                        <div class="indtextBlack-text">
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--redBg">辣</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <h4 class="indtextBlack-text--Style">
                                戀愛如花朵一樣美好、清香，但初戀都在回憶，記憶中的初戀有一點甜、有一點酸⋯難忘的味道！
                             </h4>
                        </div>
                        <img class="indtextBlack-arrow wow" src="images/arrow-bt.png" alt="產品連結內頁" width="92" height="15">
                        <img src="images/asset-15.png" alt="櫻花" class="indintro04-ele04 wow">
                    </a>
                    <div class="clear"></div>
                </article>

                <!-- 幸福椒麻拌麵區 -->
                <article class="indintro-section05">
                    <div class="indintro05-tit indintroTitArea">
                        <!-- <h1 class="indNoodleTit wow">幸福</h1> -->
                        <h1 class="indNoodleTit wow">
                            <span class="indNoodleTit--word">幸</span>
                            <span class="indNoodleTit--word">福</span>
                        </h1>
                        <h4 class="indNoodleSubTit wow">
                            <span class="indNoodleSubTit--word">椒</span>
                            <span class="indNoodleSubTit--word">麻</span>
                            <span class="indNoodleSubTit--word">拌</span>
                            <span class="indNoodleSubTit--word">麵</span>
                        </h4>
                    </div>
                    <div id="jsnoodle04" class="indintro05-noodleImgArea">
                        <img src="images/asset-31-noodle04.png" alt="煙霧" class="indintro05--smokeImg wow">
                        <img src="images/asset-32-noodle04.png" alt="幸福椒麻拌麵形象圖" class="indintro05--noodleBgImg wow">
                        <div class="wow indintro05-ele01--horizontal">
                            <img src="images/asset-16.png" class="wow indintro05-ele01--vertical" alt="img">
                        </div>
                    </div>
                    <a href="productsin.php" class="indintro05-intro indtextBlack wow">
                        <h4 class="indtextBlack-inrto">霸道品牌經典款</h4>
                        <h4 class="indtextBlack-inrto">純天然釀造黑龍醬油，回甘不死鹹</h4>
                        <div class="indtextBlack-text">
                            <div class="eleFeature-featureText">
                                <h1 class="eleFeature eleFeature--redBg">辣</h1>
                                <img src="images/red-dot-icon.png" alt="dot" class="eleFeature--dot">
                            </div>
                            <h4 class="indtextBlack-text--Style">
                                採用純天然釀造「黑龍醬油」，天然回甘不死鹹！不放辣椒精、花椒精，口感順口不刺激！<br>
                                「關廟日曬麵」非油炸麵體，不加防腐劑！自然、健康、無負擔！
                             </h4>
                        </div>
                        <img class="indtextBlack-arrow wow" src="images/arrow-bt.png" alt="產品連結內頁" width="92" height="15">
                        <img src="images/asset-19.png" class="wow indintro05-ele02" alt="img">
                    </a>
                    <div class="clear"></div>
                </article>

            </div>

            <!-- footer -->
            <?php require('footer.php') ?>
            
		</div>

	<!-- </div> -->
        
    </div>
	<!-- semantic UI 設定檔 -->
    <?php require('semantic-setting.php') ?>
    

    <!-- 首頁loading特效 -->
    <!-- 1.用法 要在最外層包一個class=loading_filter -->
    <link href="css/loader.css" rel="stylesheet"  type="text/css" media="all" />
    <script type="text/javascript" src="js/jquery.nimble.loader.js"></script>
    <script language="javascript">
        // $("body").nimbleLoader("show", {
        //     position             : "fixed",
        //     loaderClass          : "loading_bar",
        //     debug                : true,
        //     speed                : 2000,
        //     hasBackground        : true,
        //     zIndex               : 9999,
        //     backgroundColor      : "#FFFFFF",
        //     backgroundOpacity    : 1
        // });

        $(document).ready(function() {
            // $("body").nimbleLoader("hide");
            // $(".indloadingHide").css("opacity","0");
            // alert("網頁全部DOM元素載完了");

            

        });

       
    </script>
    
</body>
</html>

     