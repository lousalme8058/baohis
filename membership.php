<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">會員專區</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">會</span>
						<span class="elepageTit--word">員</span>
						<span class="elepageTit--word">專</span>
						<span class="elepageTit--word">區</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>
			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="membership.php" class="eleTab eleTab--in">
							訂單紀錄
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
						<a href="membership-profile.php" class="eleTab">
							會員資料
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
					</article>

					<!-- 訂單資料區 -->
					<div class="papmemberArea">
						<!-- 訂單列表 -->
						<article class="papmemberList">
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單日期</h6>
								<h4 class="papmemberContent-text">2020.05.24</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單編號</h6>
								<h4 class="papmemberContent-text">202005240001</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單金額</h6>
								<h4 class="papmemberContent-text">$540</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單狀態</h6>
								<h4 class="papmemberContent-text">已出貨</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單明細</h6>
								<a href="membershipin.php" class="papmemberContent-bt ptb-10">
									<img src="images/next-icon.png" alt="icon">
								</a>
							</div>
						</article>
						<!-- 訂單列表 -->
						<article class="papmemberList">
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單日期</h6>
								<h4 class="papmemberContent-text">2020.05.24</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單編號</h6>
								<h4 class="papmemberContent-text">202005240001</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單金額</h6>
								<h4 class="papmemberContent-text">$540</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單狀態</h6>
								<h4 class="papmemberContent-text">等待出貨</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單明細</h6>
								<a href="membershipin.php" class="papmemberContent-bt ptb-10">
									<img src="images/next-icon.png" alt="icon">
								</a>
							</div>
						</article>
						<!-- 訂單列表 -->
						<article class="papmemberList">
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單日期</h6>
								<h4 class="papmemberContent-text">2020.05.24</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單編號</h6>
								<h4 class="papmemberContent-text">202005240001</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單金額</h6>
								<h4 class="papmemberContent-text">$540</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單狀態</h6>
								<h4 class="papmemberContent-text typo-danger">尚未付款</h4>
							</div>
							<div class="papmemberContent">
								<h6 class="papmemberContent-tit">訂單明細</h6>
								<a href="membershipin.php" class="papmemberContent-bt">
									<h4 class="papmemberContent-text typo-danger typo-underline pb-10">前往付款</h4>
								</a>
							</div>
						</article>
						
						<div class="clear"></div>
					</div>
					
					<div class="modBtarea mt-30">
						<a href="productsli.php" class="btnBlackBt modBtarea-nextBt mb-10">
							<img src="images/next-icon.png" width="70" height="auto" alt="">
							前往購物車
						</a>
						<a href="javascript:void(0);" class="btnWhiteBt modBtarea-backBt mb-10">回首頁</a>
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     