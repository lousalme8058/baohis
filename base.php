<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patWrapper">
            
            <!-- 大視口導覽列 -->       
            <?php require('header.php') ?>

            
            <!-- footer -->
            <?php require('footer.php') ?>
            
		</div>

	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     