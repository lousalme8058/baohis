<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">會員專區</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">會</span>
						<span class="elepageTit--word">員</span>
						<span class="elepageTit--word">專</span>
						<span class="elepageTit--word">區</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">
					<!-- 頁面切換按鈕 -->
					<article class="eleTabArea">
						<!-- 頁面懸停時加eleTab--in -->
						<a href="membership-profile.php" class="eleTab eleTab--in">
							會員資料
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
						<a href="membership.php" class="eleTab">
							訂單紀錄
							<img src="images/next-icon.png" width="70" height="auto" alt="">
						</a>
					</article>

					<!-- 訂單資料區 -->
					<div class="paprofileArea">
						<!-- 訂單金額計算 -->
						<article class="paprofileArea-leftArea">
							<h3 class="mb-30">會員資料修改</h3>
							<div class="ui form">
								<div class="field fidArea fidArea--nogutter">
									<label for="姓名">姓名<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="姓名">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="聯絡電話">聯絡電話 <span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="聯絡電話 ">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="手機">手機<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="手機">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="寄送地址">寄送地址<span class="fieverti">＊</span></label>
									<select class="ui dropdown mb-15">
										<option value="">輸入縣／市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
										<option value="台北市">台北市</option>
									</select>
									<select class="ui dropdown mb-15">
										<option value="">輸入區域</option>
										<option value="中和區">中和區</option>
									</select>
									<input type="text" name="" id="" placeholder="輸入地址">
								</div>
								<a href="javascript:void(0);" class="btnRedBt modBtarea-backBt">確認修改</a>
							</div>
							
							<div class="clear"></div>
						</article>
						
						<!-- 會員密碼修改 -->
						<article class="paprofileArea-rightArea">
							<h3 class="mb-30">會員密碼修改　<span class="typo-danger">如不修改請勿輸入</span></h3>
							<div class="ui form">
								<div class="field fidArea fidArea--nogutter">
									<label for="帳號">帳號<span class="fieverti">＊</span></label>
									<input type="email" name="" id="" placeholder="abcdefg@gmail.com" readonly="">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="密碼">密碼<span class="fieverti">＊</span></label>
									<input type="password" name="" id="" placeholder="0000">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="再次確認密碼">再次確認密碼<span class="fieverti">＊</span></label>
									<input type="password" name="" id="" placeholder="0000">
								</div>
								<a href="javascript:void(0);" class="btnRedBt modBtarea-backBt">確認修改</a>
							</div>
							<div class="clear"></div>
						</article>
						<div class="clear"></div>
					</div>
					

					<!-- <div class="modBtarea mt-30">
						<a href="cart03.php" class="btnBlackBt modBtarea-nextBt mb-10">
							<img src="images/next-icon.png" width="70" height="auto" alt="">
							選擇付款方式
						</a>
						<a href="cart01.php" class="btnWhiteBt modBtarea-backBt mb-10">回購物車</a>
					</div> -->
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>

			<!-- 元素動畫 -->
			<img src="images/asset-34.png" alt="img" class="elepageAniArea02 wow">

		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     