<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
</head>
<body>
	<div class="bg">
        <!-- 小視口及置頂導覽列 -->
        <?php require('header-sml.php') ?>

		<div class="patpageWrapper">

			<div class="patBigNav--bg--page">
				<!-- 大視口導覽列 -->       
				<?php require('header.php') ?>
			</div>


			<!-- 頁面左側欄位 -->
			<aside class="patpagesidebar">
				<article class="patpagesidebar-titArea">
					<!-- 標題 -->
					<!-- <h1 class="elepageTit wow">會員登入</h1> -->
					<h1 class="elepageTit wow">
						<span class="elepageTit--word">會</span>
						<span class="elepageTit--word">員</span>
						<span class="elepageTit--word">登</span>
						<span class="elepageTit--word">入</span>
					</h1>
				</article>
				
				<!-- 元素動畫 -->
				<!-- <img src="images/asset-33.png" alt="img" class="elepageAniArea01 wow"> -->
			</aside>

			<div class="patmax_width paCartFinHeight">
				<!-- 頁面主要內容區 -->
				<div class="patpageprimary mb-50">

					<div class="paLoginArea">
						<!-- 頁面切換按鈕 -->
						<article class="eleTabArea">
							<!-- 頁面懸停時加eleTab--in -->
							<a href="login.php" class="eleTab eleTab--in">
								會員登入
								<img src="images/next-icon.png" width="70" height="auto" alt="">
							</a>
							<a href="sign-up.php" class="eleTab">
								加入會員
								<img src="images/next-icon.png" width="70" height="auto" alt="">
							</a>
						</article>

						<article class="paLogin">
							<h3 class="paLoginSocialTit mt-60 mb-20"><span>社群登入</span></h3>
							
							<div class="paLoginSociaArea mb-30">
								<a href="javascript:void(0);" class="paLoginSocia-bt">
									<img src="images/fb-bt.png" alt="facebook 登入">
								</a>
								<a href="javascript:void(0);" class="paLoginSocia-bt">
									<img src="images/google-bt.png" alt="google 登入">
								</a>
								<div class="clear"></div>
							</div>

							<h3 class="paLoginSocialTit mt-60 "><span>會員登入</span></h3>
							<div class="ui form">
								<div class="field fidArea fidArea--nogutter">
									<label for="帳號">帳號<span class="fieverti">＊</span></label>
									<input type="text" name="" id="" placeholder="帳號">
								</div>
								<div class="field fidArea fidArea--nogutter">
									<label for="密碼">密碼<span class="fieverti">＊</span></label>
									<input type="password" name="" id="" placeholder="密碼">
								</div>
							</div>
							<a href="membership.php" class="btnRedBt mb-10">登入</a>
							<a href="forget.php" class="btnWhiteBt mb-10">忘記密碼</a>
							<br>
						</article>
				
					</div>
				</div>
			</div>

			<!-- footer -->
			<?php require('footer.php') ?>


			<!-- 元素動畫 -->
			<img src="images/asset-34.png" alt="img" class="elepageAniArea02 wow">
            
		</div>
	</div>
	<!-- semantic UI 設定檔 -->
	<?php require('semantic-setting.php') ?>
</body>
</html>

     