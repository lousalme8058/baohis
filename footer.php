<div class="max_width">
	<br>
	<br>
	<!-- <article class="patFooter-shopcom">
		<p>
			提醒您，以下都是詐騙：訂單設錯、訂單金額有誤、設成分期每月扣款、重覆訂購多筆、
			宅配或超商出錯、簽收單簽成簽帳單、條碼刷錯、重覆扣款。歹徒會以更改訂單要求退費
			給您為由，要求您操作 ATM，這些都是詐騙手法。若遇可疑來電，隨時可撥打 165 反詐
			騙諮詢專線。
		</p>
	</article> -->
	<footer class="patFooter">
		<article class="patFooter-about">   
			<h6 class="patFooter-tit">ABOUT BAOHIS</h6>    
			<div class="patFooter-about--info">
				<p class="patFooter--text mb-10">
					<span class="patFooter--text--tit">ADD</span>
					嘉義市中埔鄉和美村5鄰36-88號
				</p>
				<p class="patFooter--text">
					<span class="patFooter--text--tit">E-MAIL</span>
					baohiscoltd@gmail.com
				</p>
				<p class="patFooter--text mb-10">
					<span class="patFooter--text--tit">香港經銷</span>
					志達科技股份有限公司
				</p>
				<p class="patFooter--text">
					<span class="patFooter--text--tit">香港E-MAIL</span>
					info@i-smart-Ltd.com
				</p>
			</div>
		</article>
		<article class="patFooter-contact">
			<h6 class="patFooter-tit">CONTACT US</h6>    
			<div class="patFooter-contact--info">
				<p class="patFooter--text mb-10">
					<span class="patFooter--text--tit">TEL</span>
					( 05 )  2398 - 619
				</p>
				<p class="patFooter--text">
					<span class="patFooter--text--tit">FAX</span>
					( 05 )  2398 - 582
				</p>
				<p class="patFooter--text mb-10">
					<span class="patFooter--text--tit">香港TEL</span>
					( 852 ) 2417 - 0002
				</p>
			</div>
		</article>
		<article class="patFooter-purchase">
			<h6 class="patFooter-tit">PURCHASE INFO</h6>    
			<div class="patFooter-purchase--info">
				<a href="qa.php" class="patFooter--link mb-10">
					常見問題
				</a>
				<a href="membership.php" class="patFooter--link mb-10">
					會員專區
				</a>
			</div>
		</article>
		<article class="patFooter-copyright">
			<p class="patFooter--text">
				COPYRIGHT © 2018 BAOHIS ALL RIGHTS RESERVED.
				<a href="http://www.cosmosdesign.tw" class="patFooter-copyright--design">DESIGN COSMOS</a>
			</p>
		</article>
	</footer>
	<article class="patFooter-shopcom">
		<p>
			提醒您，以下都是詐騙：訂單設錯、訂單金額有誤、設成分期每月扣款、重覆訂購多筆、宅配或超商出錯、簽收單簽成簽帳單、條碼刷錯、重覆扣款。
			歹徒會以更改訂單要求退費給您為由，要求您操作 ATM，這些都是詐騙手法。若遇可疑來電，隨時可撥打 165 反詐騙諮詢專線。
		</p>
	</article>
</div>