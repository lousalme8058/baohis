<!DOCTYPE html>	
<head>
<title>確實霸道首頁</title>
<?php require('head.php') ?>
<script>
$(document).ready(function() {
    //scroll卷軸滑動速率
    function scrollAni(){
        document.addEventListener("mousewheel", function(e){
            // console.log("開始監聽");
            e.preventDefault();
        }, { passive: false });
        //DOMMouseScroll --firefox用
        document.addEventListener("DOMMouseScroll", function(e){
            // console.log("開始監聽");
            e.preventDefault();
        }, { passive: false });
        var speed = 500;
        $(window).on("mousewheel DOMMouseScroll", function(e){
            var delta = e.originalEvent.wheelDelta / 120 || - e.originalEvent.detail/3;
            var scrollTop = $(window).scrollTop();
            var finalScroll = scrollTop - delta * speed ;
            TweenMax.to($(window), 1.2, { scrollTo : { y: finalScroll }, ease: Power2.easeOut });
        });
    };

    //在select裡不要執行卷軸滑動速率
    $(".ul").on("mousewheel DOMMouseScroll", function(e){
        //取消冒泡事件，就不會向上傳遞
        //捕獲與冒泡文件
        //https://blog.techbridge.cc/2017/07/15/javascript-event-propagation/
        event.stopPropagation();
        // console.log("滑動內卷軸");
    });

    //return 是當function在運行時，可以指定回傳一個值，當回傳值回來之後，便會跳出function，所以我們在條件判斷式中寫return false，是用來跳出該function
    var getExplorer = (function() {
        var explorer = window.navigator.userAgent,
        compare = function(s) { return (explorer.indexOf(s) >= 0); },
        ie11 = (function() { return ("ActiveXObject" in window) })();
        if (compare("MSIE") || ie11) { return 'ie'; }
        else if (compare("Firefox") && !ie11) { return 'Firefox'; }
        else if (compare("Chrome") && !ie11) { return 'Chrome'; }
        else if (compare("Opera") && !ie11) { return 'Opera'; }
        else if (compare("Safari") && !ie11) { return 'Safari'; }
    })();

    //在IE不要執行scrollAni()
    // 注意：getExplorer返回結果是一個字串，如判斷是否為IE瀏覽器
    if (getExplorer == 'ie') {
        console.log('當前瀏覽器版本：IE');  
    }else{
        scrollAni();
    };

});

</script>


</head>
<body id="test">
    <p class="ul" style="width: 200px;height: 400px;overflow: scroll;color: #FF0000;">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione laboriosam laudantium expedita molestias unde officia, facere dolorem? Doloremque consequatur sint asperiores quo expedita officiis. Labore repudiandae ipsum inventore provident consequuntur.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione laboriosam laudantium expedita molestias unde officia, facere dolorem? Doloremque consequatur sint asperiores quo expedita officiis. Labore repudiandae ipsum inventore provident consequuntur.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione laboriosam laudantium expedita molestias unde officia, facere dolorem? Doloremque consequatur sint asperiores quo expedita officiis. Labore repudiandae ipsum inventore provident consequuntur.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione laboriosam laudantium expedita molestias unde officia, facere dolorem? Doloremque consequatur sint asperiores quo expedita officiis. Labore repudiandae ipsum inventore provident consequuntur.
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione laboriosam laudantium expedita molestias unde officia, facere dolorem? Doloremque consequatur sint asperiores quo expedita officiis. Labore repudiandae ipsum inventore provident consequuntur.
    </p>
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
  Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam cupiditate quas voluptates, ullam sit dignissimos esse nihil explicabo illo optio molestias ex accusamus ducimus dolor! Quae aliquid quisquam expedita itaque!
    


</html>

     