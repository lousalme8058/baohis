<title>Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="zh-TW" />
<meta name="viewport" content="width=device-width , initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="author"  content="小宇宙資訊有限公司,www.cosmosdesign.tw" />
<meta name="COPYRIGHT" content="Copyright (c) by 小宇宙資訊有限公司,www.cosmosdesign.tw">
<meta name="keywords" Lang="EN" content=" "/>
<meta name="keywords" Lang="zh-TW" content=" " />
<!--css檔連結區-->
<link href="css/cssreset.css" rel="stylesheet" type="text/css" media="all" />
<!-- semantic UI -->
<link href="css/semantic.css" rel="stylesheet" type="text/css" media="all" />
<!-- 網站區layout -->
<link href="css/layout.css" rel="stylesheet" type="text/css" media="all" />

<!--js檔連結區-->
<script src="js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="js/jquery.md5.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<!-- js檔 -->
<script src="js/web-js.js"></script>


<!--google 雲端字型-->
<!-- Playfair字型 -->
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,600;1,600&display=swap" rel="stylesheet">
<!-- poppings 字型 -->
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500&display=swap" rel="stylesheet">
<!-- noto 思源宋體 -->
<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+TC:wght@600;700;900&display=swap" rel="stylesheet">
<!-- noto 思源黑體 -->
<link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC:500,700" rel="stylesheet">


<!-- wow.js -->
<!-- animate.css -->
<!-- <link href="css/animate.css" rel="stylesheet" type="text/css" media="all" /> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
<script src="js/wow.js"></script>
<script>
    var wow = new WOW(
     {
      boxClass: 'wow', // 要套用WOW.js縮需要的動畫class(預設是wow)
      animateClass: 'animated', // 要"動起來"的動畫(預設是animated, 因此如果你有其他動畫library要使用也可以在這裡調整)
      offset: 0, // 距離顯示多遠開始顯示動畫 (預設是0, 因此捲動到顯示時才出現動畫)
      mobile: true, // 手機上是否要套用動畫 (預設是true)
      live: true, // 非同步產生的內容是否也要套用 (預設是true, 非常適合搭配SPA)
      callback: function(box) {
      // 當每個要開始時, 呼叫這裡面的內容, 參數是要開始進行動畫特效的element DOM
     },
      scrollContainer: null // 可以設定成只套用在某個container中捲動才呈現, 不設定就是整個視窗
     }
    );
    wow.init();
</script>


<!-- animate -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/> -->

<!--載入tweenmax-->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/ScrollToPlugin.min.js"></script>





